package com.app.model;

import java.sql.Date;
import java.util.Collection;

public class JobSeeker {

	private int userid;
	private String urole;
	private String uname;
	private String uemail;
	private String ucity;
	private int unumber;
	private String uskill;
	private Date dateapplied;
	private String emptype;

	public JobSeeker(String urole, String uname, String uemail, String ucity, int unumber, String ustatus,
			Date dateapplied, String emptype) {
		super();
		//this.userid = userid;
		this.urole = urole;
		this.uname = uname;
		this.uemail = uemail;
		this.ucity = ucity;
		this.unumber = unumber;
		this.uskill= ustatus;
		this.dateapplied = dateapplied;
		this.emptype = emptype;
	}

	public JobSeeker() {
		super();
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getURole() {
		return urole;
	}

	public void setURole(String role) {
		this.urole = role;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUemail() {
		return uemail;
	}

	public void setUemail(String uemail) {
		this.uemail = uemail;
	}

	public String getUcity() {
		return ucity;
	}

	public void setUcity(String ucity) {
		this.ucity = ucity;
	}

	public int getUnumber() {
		return unumber;
	}

	public void setUnumber(int unumber) {
		this.unumber = unumber;
	}

	public String getUskill() {
		return uskill;
	}

	public void setUskill(String uskill) {
		this.uskill = uskill;
	}

	public Date getDateapplied() {
		return dateapplied;
	}

	public void setDateapplied(Date dateapplied) {
		this.dateapplied = dateapplied;
	}

	public String getEmptype() {
		return emptype;
	}

	public void setEmptype(String emptype) {
		this.emptype = emptype;
	}

	@Override
	public String toString() {
		return "JobSeeker [userid=" + userid + ", urole=" + urole + ", uname=" + uname + ", uemail=" + uemail
				+ ", ucity=" + ucity + ", unumber=" + unumber + ", dateapplied=" + dateapplied
				+ ", emptype=" + emptype + "]";
	}

	public static Object getAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	

	

	
	
	}


