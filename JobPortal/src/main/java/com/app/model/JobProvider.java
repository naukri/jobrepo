 package com.app.model;

import java.sql.Date;

public class JobProvider {
	private int jobid;
	private String prole;
	private String companyname;
	private int exprequired;
	private String skilsrequired;
	private String joblocation;
	private String recruitername;
	private String recruiternumber;
	private Date dateapplied;
	
	private JobSeeker jobseeker;
	
	public JobProvider() {}
	public JobProvider(String prole, String companyname, int exprequired, String skilsrequired,
			String joblocation, String recruitername, String recruiternumber, Date dateapplied) {
		super();
		//this.jobid = jobid;
		this.prole = prole;
		this.companyname = companyname;
		this.exprequired = exprequired;
		this.skilsrequired = skilsrequired;
		this.joblocation = joblocation;
		this.recruitername = recruitername;
		this.recruiternumber = recruiternumber;
		this.dateapplied = dateapplied;
	}
	public int getJobid() {
		return jobid;
	}
	public void setJobid(int jobid) {
		this.jobid = jobid;
	}
	public String getProle() {
		return prole;
	}
	public void setProle(String prole) {
		this.prole = prole;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public int getExprequired() {
		return exprequired;
	}
	public void setExprequired(int exprequired) {
		this.exprequired = exprequired;
	}
	public String getSkilsrequired() {
		return skilsrequired;
	}
	public void setSkilsrequired(String skilsrequired) {
		this.skilsrequired = skilsrequired;
	}
	public String getJoblocation() {
		return joblocation;
	}
	public void setJoblocation(String joblocation) {
		this.joblocation = joblocation;
	}
	public String getRecruitername() {
		return recruitername;
	}
	public void setRecruitername(String recruitername) {
		this.recruitername = recruitername;
	}
	public String getRecruiternumber() {
		return recruiternumber;
	}
	public void setRecruiternumber(String recruiternumber) {
		this.recruiternumber = recruiternumber;
	}
	public Date getDateapplied() {
		return dateapplied;
	}
	public void setDateapplied(Date dateapplied) {
		this.dateapplied = dateapplied;
	}
	@Override
	public String toString() {
		return "[jobid=" + jobid + ", prole=" + prole + ", companyname=" + companyname + ", exprequired="
				+ exprequired + ", skilsrequired=" + skilsrequired + ", joblocation=" + joblocation + ", recruitername="
				+ recruitername + ", recruiternumber=" + recruiternumber + ", dateapplied=" + dateapplied + "]";
	}
	
	
	
	
}
