package com.app.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
	static Connection con;

	public static Connection openConnection() {
		Properties properties = new Properties();
		try {
			properties.load(new FileReader("src/main/resources/jdbc.properties"));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		String drivername = (String) properties.get("oracle.driver");
		String url = (String) properties.get("oracle.url");
		String username = (String) properties.get("oracle.username");
		String password = (String) properties.get("oracle.password");

		con = null;
		try {

			Class.forName(drivername);
			try {
				con = DriverManager.getConnection(url, username, password);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e1) {
			System.out.println(e1.getMessage());
		}
		return con;

	}

	public static void closeConnection() {

		try {
			if (con != null)
				con.close();
		} catch (SQLException s1) {
			System.out.println(s1.getMessage());

		}

	}

}
