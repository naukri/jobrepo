package com.app.dao;

import java.util.List;

import com.app.exception.UserNotFoundException;
import com.app.model.JobSeeker;

public interface JobSeekerDAO {

	public void addUser(JobSeeker jobseeker);

	public boolean updateUser(int userid, int unumber, String ucity);

	public boolean deleteUser(int userid);

	public JobSeeker getUserById(int userid);

	public JobSeeker getUserBySkills(String skills);
	
	public List<JobSeeker> getAllUsers();

}
