package com.app.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.app.exception.JobNotFoundException;
import com.app.model.JobProvider;

public class JobProviderDAOImpl implements JobProviderDAO {

	Connection connection = DBConnection.openConnection();

	public void addJob(JobProvider job) {

		connection = DBConnection.openConnection();
		String sql = "insert into JobProvider(prole,skillsrequired,companyname,exprequired,joblocation,recruitername,recruiternumber,dateapplied,jobid) values(?,?,?,?,?,?,?,?,jopro_seq.nextval)";
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, job.getProle());
			statement.setString(2, job.getSkilsrequired());
			statement.setString(3, job.getCompanyname());
			statement.setInt(4, job.getExprequired());
			statement.setString(5, job.getJoblocation());
			statement.setString(6, job.getRecruitername());
			statement.setString(7, job.getRecruiternumber());
			statement.setDate(8, job.getDateapplied());
			statement.execute();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e2) {
				System.out.println(e2.getMessage());
			}
		}
	}

	public List<JobProvider> getAllJobs() {
		connection = DBConnection.openConnection();
		String sql = "select * from Jobprovider";
		PreparedStatement statement = null;
		List<JobProvider> jobList = new ArrayList<JobProvider>();
		try {
			statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				JobProvider job = new JobProvider();
				job.setCompanyname(rs.getString("companyname"));
				job.setDateapplied(rs.getDate("dateapplied"));
				job.setExprequired(rs.getInt("exprequired"));
				job.setJoblocation(rs.getString("joblocation"));
				job.setProle(rs.getString("prole"));
				job.setRecruitername(rs.getString("recruitername"));
				job.setRecruiternumber(rs.getString("recruiternumber"));
				job.setSkilsrequired(rs.getString("skillsrequired"));
				job.setJobid(rs.getInt("jobid"));
				jobList.add(job);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return jobList;
	}

	public JobProvider getJobbyId(int jobid) throws JobNotFoundException {
		connection = DBConnection.openConnection();
		String sql = "select * from Jobprovider where jobid=?";
		PreparedStatement statement = null;
		JobProvider job = new JobProvider();
		try {
			statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement.setInt(1, jobid);
			ResultSet rs = statement.executeQuery();
			if (!rs.next())
				throw new JobNotFoundException();
			rs.beforeFirst();
			while (rs.next()) {
				job.setCompanyname(rs.getString("companyname"));
				job.setDateapplied(rs.getDate("dateapplied"));
				job.setExprequired(rs.getInt("exprequired"));
				job.setJoblocation(rs.getString("joblocation"));
				job.setProle(rs.getString("prole"));
				job.setRecruitername(rs.getString("recruitername"));
				job.setRecruiternumber(rs.getString("recruiternumber"));
				job.setSkilsrequired(rs.getString("skillsrequired"));
				job.setJobid(rs.getInt("jobid"));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return job;
	}

	public JobProvider getSkilsrequired(String skilsrequired) throws JobNotFoundException {
		connection = DBConnection.openConnection();
		String sql = "select * from Jobprovider where skillsrequired=?";
		PreparedStatement statement = null;

		System.out.println("skills requried module");
		JobProvider skills = new JobProvider();
		try {
			statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement.setString(1, skilsrequired);
			ResultSet rs = statement.executeQuery();
			if (!rs.next())
				throw new JobNotFoundException();
			rs.beforeFirst();
			while (rs.next()) {
				skills.setCompanyname(rs.getString("companyname"));
				skills.setDateapplied(rs.getDate("dateapplied"));
				skills.setExprequired(rs.getInt("exprequired"));
				skills.setJoblocation(rs.getString("joblocation"));
				skills.setProle(rs.getString("prole"));
				skills.setRecruitername(rs.getString("recruitername"));
				skills.setRecruiternumber(rs.getString("recruiternumber"));
				skills.setSkilsrequired(rs.getString("skillsrequired"));
				skills.setJobid(rs.getInt("jobid"));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return skills;
	}

	
	@Override
	public JobProvider getJoblocation(String joblocation) throws JobNotFoundException {
		connection = DBConnection.openConnection();
		String sql = "select * from Jobprovider where joblocation=?";
		PreparedStatement statement = null;

		System.out.println("location requried module");
		JobProvider location = new JobProvider();
		try {
			statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement.setString(1, joblocation);
			ResultSet rs = statement.executeQuery();
			if (!rs.next())
				throw new JobNotFoundException();
			rs.beforeFirst();
			while (rs.next()) {
				location.setCompanyname(rs.getString("companyname"));
				location.setDateapplied(rs.getDate("dateapplied"));
				location.setExprequired(rs.getInt("exprequired"));
				location.setJoblocation(rs.getString("joblocation"));
				location.setProle(rs.getString("prole"));
				location.setRecruitername(rs.getString("recruitername"));
				location.setRecruiternumber(rs.getString("recruiternumber"));
				location.setSkilsrequired(rs.getString("skillsrequired"));
				location.setJobid(rs.getInt("jobid"));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return location;
	}

	@Override
	public JobProvider getJobByEmpType(String exp) {
		// TODO Auto-generated method stub
		connection = DBConnection.openConnection();
		String sql = "select * from Jobprovider where exprequired=?";
		PreparedStatement statement = null;
		JobProvider location = new JobProvider();
		try {
			statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement.setString(1, exp);
			ResultSet rs = statement.executeQuery();
			if (!rs.next())
				throw new JobNotFoundException();
			rs.beforeFirst();
			while (rs.next()) {
				location.setCompanyname(rs.getString("companyname"));
				location.setDateapplied(rs.getDate("dateapplied"));
				location.setExprequired(rs.getInt("exprequired"));
				location.setJoblocation(rs.getString("joblocation"));
				location.setProle(rs.getString("prole"));
				location.setRecruitername(rs.getString("recruitername"));
				location.setRecruiternumber(rs.getString("recruiternumber"));
				location.setSkilsrequired(rs.getString("skillsrequired"));
				location.setJobid(rs.getInt("jobid"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return location;
	}

	@Override
	public boolean updateJobProvider(int jobid, String recruitername, String skillsrequired, String recruiternumber,
			Date dateapplied) {
		Connection connection = DBConnection.openConnection();
		String sql = "update JobProvider set recruitername=?,skillsrequired=?,dateapplied=? where jobid=?";

		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, recruitername);
			statement.setString(2, skillsrequired);
			statement.setDate(3, dateapplied);
			statement.setInt(4, jobid);
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				System.out.println(e1.getMessage());
			}

		}
		return false;

	}


@Override
public JobProvider getUserByEmpType(String emptype) {
	// TODO Auto-generated method stub
	return null;
}
}
