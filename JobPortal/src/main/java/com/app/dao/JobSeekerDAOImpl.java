package com.app.dao;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.app.exception.UserNotFoundException;
import com.app.model.JobSeeker;

public class JobSeekerDAOImpl implements JobSeekerDAO {

	Connection con;

	public void addUser(JobSeeker jobseeker) {
		con = DBConnection.openConnection();
		String sql = "insert into JobSeeker(userid,urole,uname,uemail,ucity,unumber,uskill,dateapplied,emptype) values(?,?,?,?,?,?,?,?,?)";
		PreparedStatement st = null;

		try {
			st = con.prepareStatement(sql);
			st.setInt(1, jobseeker.getUserid());
			st.setString(2, jobseeker.getURole());
			st.setString(3, jobseeker.getUname());
			st.setString(4, jobseeker.getUemail());
			st.setString(5, jobseeker.getUcity());
			st.setInt(6, jobseeker.getUnumber());
			st.setString(7, jobseeker.getUskill());
			st.setDate(8, jobseeker.getDateapplied());
			st.setString(9, jobseeker.getEmptype());

			st.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());

		}
	}

	public boolean updateUser(int userid, int unumber, String ucity) {
		Connection connection = DBConnection.openConnection();
		String sql = "update JobSeeker set ucity=?,unumber=? where userid=?";

		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, ucity);
			statement.setInt(2, unumber);
			statement.setInt(3, userid);
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				System.out.println(e1.getMessage());
			}

		}
		return false;

	}

	public boolean deleteUser(int userid) {
		con = DBConnection.openConnection();
		String sql = "delete from jobseeker where userid=?";
		PreparedStatement st = null;
		JobSeeker book = new JobSeeker();
		boolean result = true;
		try {
			st=con.prepareStatement(sql);
			st.setInt(1, userid);
			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				throw new UserNotFoundException("Id not found");
			}
			result = st.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (UserNotFoundException e) {
			System.out.println(e.getMessage());
			try {
				throw e;
			} catch (UserNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			try {
				if (st != null) {
					st.close();
				}
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return result;

	}
	public JobSeeker getUserById(int userid) {
		con= DBConnection.openConnection();
		String sql= "select * from jobseeker where userid=?";
		//boolean result= false;
		PreparedStatement ps= null;
		JobSeeker job = new JobSeeker();
		try {
			ps= con.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setInt(1, userid);
			ResultSet rs= ps.executeQuery();
			if(!rs.next()) {
				throw new UserNotFoundException("ID not Found");
			}
			rs.beforeFirst();
			while(rs.next()) {
				job.setUserid(rs.getInt("userid"));
				job.setUnumber(rs.getInt("unumber"));
				job.setUname(rs.getString("uname"));
				job.setURole(rs.getString("urole"));
			job.setUskill(rs.getString("uskill"));
			
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch( UserNotFoundException e) {
			System.out.println(e.getMessage());
			
		}
		finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		
		}
		return job;
	}
	public List<JobSeeker> getAllUsers() {
		con= DBConnection.openConnection();
		String sql= "select * from jobseeker";
		//boolean result= false;
		PreparedStatement ps= null;
		
		List<JobSeeker> jobList= new ArrayList<>();
		try {
			ps= con.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			
			ResultSet rs= ps.executeQuery();
			
			rs.beforeFirst();
			while(rs.next()) {
				JobSeeker job = new JobSeeker();
				job.setUserid(rs.getInt("userid"));
			job.setUname(rs.getString("uname"));
				job.setUskill(rs.getString("ustatus"));
			job.setUemail(rs.getString("uemail"));
			job.setUcity(rs.getString("ucity"));
				job.setUnumber(rs.getInt("unumber"));
				jobList.add(job);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		
		}
		return jobList;
	}

	@Override
	public JobSeeker getUserBySkills(String skills) {
		// TODO Auto-generated method stub
		return null;
	}


}

