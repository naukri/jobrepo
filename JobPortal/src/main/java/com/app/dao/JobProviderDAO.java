
package com.app.dao;

import java.sql.Date;
import java.util.List;

import com.app.exception.JobNotFoundException;
import com.app.model.JobProvider;
import com.app.model.JobSeeker;

public interface JobProviderDAO {

	void addJob(JobProvider job);

	List<JobProvider> getAllJobs();

	JobProvider getJobbyId(int jobid) throws JobNotFoundException;

	public JobProvider getJobByEmpType(String exp);


	public JobProvider getUserByEmpType(String emptype);


	JobProvider getSkilsrequired(String skilsrequired) throws JobNotFoundException;

	JobProvider getJoblocation(String joblocation) throws JobNotFoundException;
	
	public boolean updateJobProvider(int jobid, String recruitername, String skillsrequired, String recruiternumber, Date dateapplied);

}
