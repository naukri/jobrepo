package com.app.service;

import java.sql.Date;
import java.util.List;

import com.app.exception.JobNotFoundException;
import com.app.model.JobProvider;

public interface JobProviderService {

	
	

	void addJob(JobProvider job);

	List<JobProvider> getAllJobs();

	JobProvider getJobbyId(int jobid) throws JobNotFoundException;

	JobProvider getSkilsrequired(String skilsrequired) throws JobNotFoundException;

	JobProvider getJoblocation(String joblocation) throws JobNotFoundException;

	public boolean updateJobProvider(int jobid, String recruitername, String skillsrequired, String recruiternumber,
			Date dateapplied);

	JobProvider getJobByEmpType(String exp) throws JobNotFoundException;

}
