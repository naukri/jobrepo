package com.app.service;

import java.util.List;

import com.app.model.JobSeeker;

public interface JobSeekerService {

	public void addUser(JobSeeker jobseeker);

	public boolean updateUser(int userid,int unumber,String ucity);
	public boolean deleteUser(int userid);
	public List<JobSeeker> getAllUsers();
	public JobSeeker getUserById(int userid);
	public JobSeeker getUserBySkills(String skills);
	public JobSeeker getUserByEmpType(String emptype);
	
}
