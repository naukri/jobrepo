package com.app.servieimpl;

import java.sql.Connection;
import java.util.List;
import java.util.stream.Collectors;


import com.app.dao.DBConnection;
import com.app.dao.JobSeekerDAO;
import com.app.dao.JobSeekerDAOImpl;
import com.app.exception.UserNotFoundException;
import com.app.model.JobSeeker;
import com.app.service.JobSeekerService;

public class JobSeekerServiceImpl implements  JobSeekerService {
	
	Connection con=DBConnection.openConnection();
	
	JobSeekerDAO jobseekerdao=new JobSeekerDAOImpl();

	public void addUser(JobSeeker jobseeker) {
		jobseekerdao.addUser(jobseeker);
		
	}

	public boolean updateUser(int userid,int unumber,String ucity) {
		
		jobseekerdao.updateUser(userid,unumber, ucity);
		return false;
	}

	public boolean deleteUser(int userid) {
		// TODO Auto-generated method stub
		jobseekerdao.deleteUser(userid);
		return false;
	}

	public JobSeeker getUserById(int userid) {
		 JobSeeker job= jobseekerdao.getUserById(userid);
		/*if(job==null) {
			try {
				throw new UserNotFoundException("Invalid id");
			} catch (UserNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
		return job;
	
	}


	public JobSeeker getUserBySkills(String skills) {
		// TODO Auto-generated method stub
		return null;
	}

	public JobSeeker getUserByEmpType(String emptype) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<JobSeeker> getAllUsers() {
		List<JobSeeker> jobList= jobseekerdao.getAllUsers()
				.stream()
				.sorted((jobseeker, jobseeker1)->{
					return jobseeker1.getUcity().compareTo(jobseeker.getUcity());
				}
				).collect(Collectors.toList());
				
	return jobList;
		
	}

	

}
