package com.app.servieimpl;

import java.sql.Date;
import java.util.List;

import com.app.dao.JobProviderDAO;
import com.app.dao.JobProviderDAOImpl;
import com.app.exception.JobNotFoundException;
import com.app.model.JobProvider;
import com.app.service.JobProviderService;

public class JobProviderServiceImpl implements JobProviderService {

	JobProviderDAO jobproDAO = new JobProviderDAOImpl();

	public void addJob(JobProvider job) {

		jobproDAO.addJob(job);

	}

	public List<JobProvider> getAllJobs() {
		return jobproDAO.getAllJobs();
	}

	public JobProvider getJobbyId(int jobid) throws JobNotFoundException {
		return jobproDAO.getJobbyId(jobid);
	}

	public JobProvider getSkilsrequired(String skilsrequired) throws JobNotFoundException {
		return jobproDAO.getSkilsrequired(skilsrequired);

	}


	@Override
	public JobProvider getJobByEmpType(String exp) throws JobNotFoundException {
		return jobproDAO.getJobByEmpType(exp);
	}

	public JobProvider getJoblocation(String joblocation) throws JobNotFoundException {
		return jobproDAO.getJoblocation(joblocation);
	}

	public boolean updateJobProvider(int jobid, String recruitername, String skillsrequired, String recruiternumber,
			Date dateapplied) {
		jobproDAO.updateJobProvider(jobid, recruitername, skillsrequired, recruiternumber, dateapplied);
		return false;
	}

}
